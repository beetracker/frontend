// import {Action, ActionReducer} from '@ngrx/store';
import {Colony} from '../models';
import {ColonyAction} from '../actions/colony.action';

export function colonyReducer (state: Colony[] = [], action: ColonyAction) {
    if (!(action instanceof ColonyAction)) {
        return state;
    }
    switch (action.type) {
        case 'Add':
            for (const colony of state) {
                if (action.payload.id === colony.id) {
                    Object.assign(colony, action.payload);
                    return state;
                }
            }
            state.push(action.payload);
            return state;
        case 'Remove':
            return state.filter(item => item.id === action.payload.id);
        default:
            return state;
    }
}
