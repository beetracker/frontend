// import {Action, ActionReducer} from '@ngrx/store';
import {Group} from '../models';
import {GroupAction} from '../actions/group.action';

export function groupReducer (state: Group[] = [], action: GroupAction) {
    if (!(action instanceof GroupAction)) {
        return state;
    }
    switch (action.type) {
        case 'Add':
            for (const group of state) {
                if (action.payload.id === group.id) {
                    Object.assign(group, action.payload);
                    return state;
                }
            }
            state.push(action.payload);
            return state;
        case 'Remove':
            return state.filter(item => item.id === action.payload.id);
        default:
            return state;
    }
}
