import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ColonyEditPage} from './colony-edit';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    ColonyEditPage,
  ],
  imports: [
    IonicPageModule.forChild(ColonyEditPage),
    TranslateModule.forChild(),
  ],
})
export class ColonyEditPageModule {}
