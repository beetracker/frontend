import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Store} from '@ngrx/store';
import {AppState} from '../../services/app-state';
import {Colony, Group} from '../../models';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ColonyAction} from '../../actions/colony.action';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs/Observable';

@IonicPage({
  segment: 'colony/:id/edit'
})
@Component({
  selector: 'page-colony-edit',
  templateUrl: 'colony-edit.html',
})
export class ColonyEditPage {

  @ViewChild('form') nav: any;

  public colony: Colony = null;

  public groups: Observable<Group[]>;

  public colonyForm: FormGroup;

  private createFormFromColony () {
    this.colonyForm = this.formBuilder.group({
      name: new FormControl(this.colony.name, [
        Validators.required,
        Validators.minLength(4),
      ]),
      isHive: new FormControl(this.colony.isHive),
      isNucleus: new FormControl(this.colony.isNucleus),
      locationName: new FormControl(this.colony.locationName),
      Group: new FormControl(this.colony.Group),
      additionalInformation: new FormControl(this.colony.additionalInformation),
    });
  }

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    public store: Store<AppState>,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
  ) {
    this.groups = this.store.select('groups');
    this.colony = new Colony();
    this.createFormFromColony();
  }

  public submitForm () {
    if (this.colonyForm.valid) {
      for (const item of Object.keys(this.colonyForm.controls)) {
        this.colony[item] = this.colonyForm.get(item).value;
      }
      this.store.dispatch(new ColonyAction('Add', this.colony));

      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot('ColonyDetailPage', {
          id: this.colony.id,
        });
      }
    }
  }

  async ionViewDidLoad () {
    const id = this.navParams.get('id');
    if ((typeof id === 'string') && (id.length > 0)) {
      this.store.subscribe((item: AppState) => {
        const colonies = item.colonies;
        if (Array.isArray(colonies) && (colonies.length > 0)) {
          for (const colony of colonies) {
            if (colony.id === id) {
              this.colony = colony;
              this.createFormFromColony();
            }
          }
        }
      });
    }
  }
}
