import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ColonyDetailPage } from './colony-detail';

@NgModule({
    declarations: [
        ColonyDetailPage,
    ],
    imports: [
        IonicPageModule.forChild(ColonyDetailPage),
    ],
})
export class ColonyDetailPageModule { }
