import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Colony} from '../../models/index';
import {Store} from '@ngrx/store';
import {AppState} from '../../services/app-state';

import 'rxjs/add/operator/map';

/**
 * Generated class for the ColonyDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
    segment: 'colony/:id'
})
@Component({
    selector: 'colony-detail',
    templateUrl: 'colony-detail.html',
})
export class ColonyDetailPage {

    public colony: Colony = null;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public store: Store<AppState>,
    ) {
        this.colony = new Colony();
    }

    async ionViewDidLoad () {
        const id = this.navParams.get('id');
        this.store.subscribe((item: AppState) => {
            const colonies = item.colonies;
            if (Array.isArray(colonies) && (colonies.length > 0)) {
                for (const colony of colonies) {
                    if (colony.id === id) {
                        this.colony = colony;
                    }
                }
            }
        });
    }

    public edit () {
        this.navCtrl.push('ColonyEditPage', {
            id: this.colony.id,
        });
    }

}
