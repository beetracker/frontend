import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Colony, Group} from '../../models';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {AppState} from '../../services/app-state';
import {TranslateService} from '@ngx-translate/core';

import 'rxjs/add/operator/filter';

@IonicPage()
@Component({selector: 'colony-list', templateUrl: 'colony-list.html'})
export class ColonyListPage {
    public items: Observable<Colony[]>;
    public group: Group;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public store: Store<AppState>,
        public translate: TranslateService,
    ) {
        if (this.navParams.get('group')) {
            this.group = this.navParams.get('group');
        } else {
            this.group = null;
        }
        this.items = this
            .store
            .select('colonies')
            .map(colony => colony.filter(item => {
                if (!this.group) {
                    return true;
                }

                if (!item.Group) {
                    return false;
                }

                return this.group.id === item.Group.id;
            }));
    }

    public addColony (): void {
        this.navCtrl.push('ColonyEditPage');
    }
}
