import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ColonyListPage} from './colony-list';
import {ComponentsModule} from '../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
    declarations: [ColonyListPage],
    imports: [
        IonicPageModule.forChild(ColonyListPage),
        ComponentsModule,
        TranslateModule.forChild(),
    ],
})
export class ColonyListPageModule {}
