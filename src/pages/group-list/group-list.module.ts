import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {GroupListPage} from './group-list';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    GroupListPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupListPage),
    TranslateModule.forChild(),
  ],
})
export class GroupListPageModule {}
