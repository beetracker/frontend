import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Store} from '@ngrx/store';
import {AppState} from '../../services/app-state';
import {TranslateService} from '@ngx-translate/core';
import {Group} from '../../models';
import {Observable} from 'rxjs/Observable';
import {GroupAction} from '../../actions/group.action';

/**
 * Generated class for the GroupListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-group-list',
  templateUrl: 'group-list.html',
})
export class GroupListPage {

  public groups: Observable<Group[]>;

  constructor (
    public navCtrl: NavController,
    public navParams: NavParams,
    public store: Store<AppState>,
    public translate: TranslateService,
    public alertCtrl: AlertController,
  ) {
    this.groups = this
      .store
      .select('groups');
  }

  public async addGroup () {
    let prompt = this.alertCtrl.create({
      title: this.translate.instant('groupList.modal.title'),
      message: this.translate.instant('groupList.modal.message'),
      inputs: [
        {
          name: 'name',
          placeholder: this.translate.instant('groupList.modal.name'),
          min: 3,
        },
      ],
      buttons: [
        {
          text: this.translate.instant('groupList.modal.cancel'),
          handler: () => {
          }
        },
        {
          text: this.translate.instant('groupList.modal.save'),
          handler: data => {
            if (data.name.length > 0) {
              const group = new Group();
              group.name = data.name;
              this.store.dispatch(GroupAction.AddGroup(group));
            } else {
              return false;
            }
          }
        }
      ]
    });

    prompt.present();
  }

  public editGroup (group: Group) {
    let prompt = this.alertCtrl.create({
      title: this.translate.instant('groupList.modal.title'),
      message: this.translate.instant('groupList.modal.message'),
      inputs: [
        {
          name: 'name',
          placeholder: this.translate.instant('groupList.modal.name'),
          value: group.name,
          min: 3,
        },
      ],
      buttons: [
        {
          text: this.translate.instant('groupList.modal.cancel'),
          handler: () => {
          }
        },
        {
          text: this.translate.instant('groupList.modal.save'),
          handler: data => {
            if (data.name.length > 0) {
              group.name = data.name;
              this.store.dispatch(GroupAction.AddGroup(group));
            } else {
              return false;
            }
          }
        }
      ]
    });

    prompt.present();
  }
}
