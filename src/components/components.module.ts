import { NgModule } from '@angular/core';
import { ColonyListItemComponent } from './colony-list-item/colony-list-item';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
    declarations: [ColonyListItemComponent],
    imports: [
        IonicPageModule.forChild(ComponentsModule),
    ],
    exports: [ColonyListItemComponent]
})
export class ComponentsModule { }
