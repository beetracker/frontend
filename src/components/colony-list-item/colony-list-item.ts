import {Component, Input} from '@angular/core';
import {Colony} from '../../models/index';
import {NavController} from 'ionic-angular';

/**
 * Generated class for the ColonyListItemComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: 'colony-list-item',
    templateUrl: 'colony-list-item.html'
})
export class ColonyListItemComponent {

    @Input()
    public colony: Colony;

    constructor (
        public navCtrl: NavController
    ) {
    }

    public async goToColony () {
        this.navCtrl.setRoot('ColonyDetailPage', {
            id: this.colony.id,
        });
    }

}
