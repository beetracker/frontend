import {Action} from '@ngrx/store';
import {Injectable} from '@angular/core';
import {Group} from '../models';

@Injectable()
export class GroupAction implements Action {
    public type: string;
    public payload: Group;

    constructor (type: string, colony: Group) {
        this.type = type;
        this.payload = colony;
    }

    public static AddGroup (group: Group) {
        return new GroupAction('Add', group);
    }

    public static RemoveColony (group: Group) {
        return new GroupAction('Remove', group);
    }
}
