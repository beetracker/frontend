import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Colony } from '../models';

@Injectable()
export class ColonyAction implements Action {
    public type: string;
    public payload: Colony;

    constructor(type: string, colony: Colony) {
        this.type = type;
        this.payload = colony;
    }

    public static AddColony(colony: Colony) {
        return new ColonyAction('Add', colony);
    }

    public static RemoveColony(colony: Colony) {
        return new ColonyAction('Remove', colony);
    }
}
