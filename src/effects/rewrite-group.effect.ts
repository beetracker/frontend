import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Actions, Effect} from '@ngrx/effects';
import {GroupAction} from '../actions/group.action';
import {of} from 'rxjs/observable/of';
import {Store} from '@ngrx/store';
import {AppState} from '../services/app-state';
import {ColonyAction} from '../actions/colony.action';

@Injectable()
export class RewriteUsedGroups {
    @Effect() login$: Observable<GroupAction> = this.actions$.ofType<GroupAction>('Add')
        .do(action => {
            this.store.select('colonies').subscribe(colonies => {
                for (const colony of colonies) {
                    if ((colony.Group) && (colony.Group.id === action.payload.id)) {
                        colony.Group = action.payload;
                        this.store.dispatch(ColonyAction.AddColony(colony));
                    }
                }
            });
        })
        .mergeMap(_item => {
            return of();
        });

    constructor (
        private actions$: Actions,
        public store: Store<AppState>,
    ) {}
}
