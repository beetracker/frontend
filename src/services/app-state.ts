import {Colony, Group} from '../models';

export interface AppState {
    groups: Group[];
    colonies: Colony[];
}
