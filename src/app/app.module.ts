import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StoreModule} from '@ngrx/store';
import {colonyReducer} from '../reducers/colony.reducer';

import {IonicStorageModule} from '@ionic/storage';
import {Deeplinks} from '@ionic-native/deeplinks';
import {ColonyListPageModule} from '../pages/colony-list/colony-list.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {Http, HttpModule} from '@angular/http';
import {groupReducer} from '../reducers/group.reducer';
import {EffectsModule} from '@ngrx/effects';
import {RewriteUsedGroups} from '../effects/rewrite-group.effect';
import {HockeyApp} from "ionic-hockeyapp";

// AoT requires an exported function for factories
export function HttpLoaderFactory (http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        MyApp,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp),
        StoreModule.forRoot({
            colonies: colonyReducer,
            groups: groupReducer,
        }),
        IonicStorageModule.forRoot({
            name: '__mydb',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        ColonyListPageModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        }),
        EffectsModule.forRoot([RewriteUsedGroups])
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
    ],
    providers: [
        Deeplinks,
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        HockeyApp,
    ]
})
export class AppModule {}
