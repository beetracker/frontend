import {Observable} from 'rxjs/Observable';
import {Component, NgZone, ViewChild} from '@angular/core';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Store} from '@ngrx/store';
import {AppState} from '../services/app-state';
import {ColonyAction} from '../actions/colony.action';
import {Colony, Group} from '../models';
import {Nav, Platform} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Deeplinks} from '@ionic-native/deeplinks';
import {ColonyListPage} from '../pages/colony-list/colony-list';
import {TranslateService} from '@ngx-translate/core';
import {GroupAction} from '../actions/group.action';
import {colonyReducer} from '../reducers/colony.reducer';
import {groupReducer} from '../reducers/group.reducer';
import {HockeyApp} from 'ionic-hockeyapp';

@Component({templateUrl: 'app.html'})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = 'ColonyListPage';

    public items: Observable<Group[]>;

    constructor (
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public store: Store<AppState>,
        private storage: Storage,
        private zone: NgZone,
        private deeplinks: Deeplinks,
        public translate: TranslateService,
        private hockeyapp: HockeyApp,
    ) {
        translate.addLangs(['en', 'de']);
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');
        translate.use('de');
        this.store.addReducer('colonies', colonyReducer);
        this.store.addReducer('groups', groupReducer);
        this.initializeApp();
    }

    public async initializeApp () {
        await this.platform.ready();
        this.hockeyapp.start('cb82b1a34fb747629da47d8d82070436', 'cb82b1a34fb747629da47d8d82070436', true, true);
        this.statusBar.styleDefault();
        this.splashScreen.hide();

        const colonies = await this.storage.get('colonies');
        const groups = await this.storage.get('groups');

        this.zone.run(() => {
            this.deeplinks.routeWithNavController(this.nav, {
                '/': ColonyListPage,
                '/about': 'AboutPage',
                '/colony/:productId': 'ColonyDetailPage'
            }).subscribe((match) => {
                // match.$route - the route we matched, which is the matched entry from the arguments to route()
                // match.$args - the args passed in the link
                // match.$link - the full link data
                console.log('Successfully matched route', match);
            }, (nomatch) => {
                // nomatch.$link - the full link data
                console.error('Got a deeplink that didn\'t match', nomatch);
            });

            if (Array.isArray(colonies)) {
                for (const _colony of colonies) {
                    const colony = new Colony();
                    Object.assign(colony, _colony);

                    this.store.dispatch(ColonyAction.AddColony(colony));
                }
            }

            if (Array.isArray(groups)) {
                for (const _group of groups) {
                    const group = new Group();
                    Object.assign(group, _group);

                    this.store.dispatch(GroupAction.AddGroup(group));
                }
            }

            this.items = this.store.select('groups');

            this.store.subscribe(async (item: AppState) => {
                await this
                    .storage
                    .set('colonies', item.colonies);

                await this
                    .storage
                    .set('groups', item.groups);
            });
        });
    }

    openPage (page) {
        // Reset the content nav to have just this page we wouldn't want the back button
        // to show in this scenario
        this
            .nav
            .setRoot(page);
    }

    public openGroupPage (group: Group) {
        this
            .nav
            .setRoot('ColonyListPage', {group});
    }
}
