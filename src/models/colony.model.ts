import {Base, Group, UUID} from '.';

export class Colony extends Base {
    public name: string;
    public locationName?: string;
    public location: {
        lat: number;
        lon: number;
    };

    public additionalInformation?: string;
    public queenYear?: number;
    public numQueenPad?: number;
    public isHive: boolean = false;
    public isNucleus: boolean = false;

    public groupId: UUID;
    public Group?: Group;

    public get formatedAdditionalInformation (): string {
        if (this.additionalInformation) {
            return this.additionalInformation.replace(/\n/g, '<br />');
        } else {
            return null;
        }
    }

    constructor () {
        super();
        this.location = {
            lat: null,
            lon: null,
        };
    }
}
