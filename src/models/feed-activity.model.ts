import { Activity, Food, FoodUnitType, UUID } from './index';

export class FeedActivity extends Activity {
    public foodId: UUID;
    public Food: Food;
    public amount: number;
    public unit: FoodUnitType;
}
