import { Base, Colony, UUID } from '.';

export abstract class Activity extends Base {
    public colonyId: UUID;
    public Colony: Colony;
    public weather?: {
        description: string;
        temperature: number;
    };
}
