import {UUID} from '.';
import * as uuid from 'uuid';

export abstract class Base {
    public id: UUID;

    public createdAt: Date;
    public updatedAt?: Date;

    public constructor () {
        this.id = uuid();
    }
}
